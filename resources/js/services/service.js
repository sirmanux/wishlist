const api = {
    getProducts: () =>
        axios.get('/getProducts'),
    getData: () =>
        axios.get('/getData'),
    addProduct: id =>
		axios.post('/add_product/' + id),
    getWishLists: listKey => 
        axios.get('/getWishlists/' + listKey),
	deleteProduct: id =>
		axios.delete('/delete_product/' + id),
    getShareUrl: () =>
        axios.get('/getShareUrl'),
}

export default api;
