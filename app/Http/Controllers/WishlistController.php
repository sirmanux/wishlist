<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use App\Wishlist;
use Auth;
use App\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;


class WishlistController extends Controller
{
    public function show($key = null)
    {
        if ($key == null) {
            $user = Auth::user();
            if ($user === null) {
                return redirect()->guest('/');
            }
        }

        return view('wishlist', array('key' => $key));
    }

    public function getWishlists($key = null)
    {
        if ($key == null) {
            $user = Auth::user();
            $wishlist = Wishlist::with('product')->where('user_id', '=', $user->id)->get();

            return array('wishlist' => $wishlist, 'user' => $user, 'listOwner' => true);
        } else {
            $user = User::where('ukey', '=', $key)->first();
            $wishlist = Wishlist::with('product')->where('user_id', '=', $user->id)->get();

            return array('wishlist' => $wishlist);
        }
    }

    public function store($pid)
    {
        $message['text'] = "Item added to your wishlist";
        $message['type'] = "success";
        $user = Auth::user();
        $uid = $user->id;
        $user = User::where('id', '=', $uid)->first();
        $product = Product::where('id', '=', $pid)->first();
        if ($user !== null && $product !== null) {
            $wishlist['user_id'] = $uid;
            $wishlist['product_id'] = $pid;

            $alreadyInserted = Wishlist::where('product_id', '=', $pid)->where('user_id', '=', $uid)->first();
            if (!empty($alreadyInserted)) {
                $message['text'] = "Item already in your wishlist";
                $message['type'] = "error";
            } else {
                \App\Wishlist::firstOrCreate(['user_id' => $uid, 'product_id' => $pid], $wishlist);
            }

        } else {
            $message = "Error when creating.";
        }

        return $message;
    }

    public function destroy($pid)
    {
        $message = array();
        $message['text'] = "Product deleted";
        $message['type'] = "success";
        $wishlist = Wishlist::where('id', '=', $pid)->first();
        if ($wishlist !== null) {
            $wishlist->delete();
        } else {
            $message['text'] = "The product you are trying to delete does not exist";
            $message['type'] = "error";
        }

        return $message;
    }

    public function getShareUrl(){
        $user = Auth::user();
        $share_url = env('APP_URL') . "/wishlist/" . $user->ukey;

        return array('shareUrl' => $share_url);
    }

}
