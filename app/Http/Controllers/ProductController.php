<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Auth;

class ProductController extends Controller
{
    public function showAllProducts(){
        return view('allproducts');
    }

    public function getAllProducts(){
        $user = Auth::user();
        $products = Product::all();

        return array('products' => $products, 'user' => $user);
    }
}
