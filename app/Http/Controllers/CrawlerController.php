<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Goutte;

class CrawlerController extends Controller
{
    public function getData()
    {
        $products = array();
        $urls = array(env('DISHWASHERS_URL'), env('SMALL_APPLIANCES_URL'));
        $diff = new \stdClass();

        $result = DB::table('last_sync')->orderBy('created_at', 'desc')->first();
        if ($result !== NULL) {
            $last_sync = new \DateTime($result->created_at);
            $now = new \DateTime();
            $diff = $now->diff($last_sync);
        }

        if ($result === NULL || (isset($diff->h) && $diff->h >= env('SYNC_TIME'))) {
            $data = array();
            $data['created_at'] = new \DateTime();
            DB::table('last_sync')->insert(array($data));
            foreach ($urls as $url) {
                $crawler = Goutte::request('GET', $url);
                $pages_arr = explode(' ', trim($crawler->filter('.products-count')->text()));
                $pages = ceil(intval($pages_arr[1]) / 20);

                for ($i = 1; $i <= $pages; $i++) {
                    $crawler = Goutte::request('GET', $url . $i);
                    $products = array_merge($products, $crawler->filter('.search-results-product')->each(function ($node) {
                        $product = array();
                        $product['img'] = count($node->filter('.product-image img')) > 0 ? trim($node->filter('.product-image img')->attr('src')) : null;
                        $product['brand'] = count($node->filter('.article-brand')) > 0 ? trim($node->filter('.article-brand')->attr('src')) : null;
                        $product['title'] = count($node->filter('.product-description h4 a')) > 0 ? trim($node->filter('.product-description h4 a')->text()) : '';
                        $product['warranty'] = count($node->filter('.sales-container img')) > 0 ? trim($node->filter('.sales-container img')->attr('src')) : null;
                        $descList = count($node->filter('.result-list-item-desc-list li')) > 0 ? $node->filter('.result-list-item-desc-list li')->each(function ($item) {
                            return trim($item->text());
                        }) : null;
                        $product['descList'] = $descList !== null ? json_encode($descList) : null;
                        $product['previousPrice'] = count($node->filter('.price-previous')) > 0 ? trim($node->filter('.price-previous')->text()) : null;
                        $product['price'] = count($node->filter('.product-description h3')) > 0 ? floatval(str_replace(array('€', ','), '', trim($node->filter('.product-description h3')->text()))) : null;
                        $moreInfo = count($node->filter('.item-info-more')) > 0 ? $node->filter('.item-info-more')->each(function ($item) {
                            return trim($item->text());
                        }) : null;
                        $product['moreInfo'] = $moreInfo !== null ? json_encode($moreInfo) : null;
                        $product['moreInfo'] = $moreInfo !== null ? json_encode($moreInfo) : null;
                        $product['freeDelivery'] = count($node->filter('.product-free-delivery')) > 0 ? 1 : 0;

                        return $product;
                    }));
                }
            }

            if (!empty($products)) {
                /* In case we want to delete the products that were deleted on the source website */
                // \App\Product::whereNotIn('title', array_column($products, 'title'))->delete();
                foreach ($products as $product) {
                    \App\Product::updateOrCreate(
                        ['title' => $product['title']],
                        $product
                    );
                }
            }

            echo "Sync done.";
        }
        else{
            echo "Nothing to sync.";
        }
    }
}
