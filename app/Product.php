<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['img', 'brand', 'title', 'warranty', 'descList', 'previousPrice', 'price', 'moreInfo', 'freeDelivery'];

    public function wishlist(){
        return $this->hasMany(Wishlist::class);
    }
}
