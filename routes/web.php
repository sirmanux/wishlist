<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProductController@showAllProducts');
Route::get('/getProducts', 'ProductController@getAllProducts');
Route::get('/getData', 'CrawlerController@getData');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/wishlist/{key?}/', 'WishlistController@show')->name('wishlist');
Route::get('/getWishlists/{key?}/', 'WishlistController@getWishlists');
Route::get('/getShareUrl', 'WishlistController@getShareUrl');

Route::middleware(['auth'])->group(function () {
    Route::post('/add_product/{pid}', 'WishlistController@store');
    Route::delete('/delete_product/{id}', 'WishlistController@destroy');
});