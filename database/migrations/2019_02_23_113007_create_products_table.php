<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->text('img')->nullable();
            $table->text('brand')->nullable();
            $table->string('title');
            $table->text('warranty')->nullable();
            $table->text('descList')->nullable();
            $table->string('previousPrice')->nullable();
            $table->decimal('price')->nullable();
            $table->text('moreInfo')->nullable();
            $table->tinyInteger('freeDelivery')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
